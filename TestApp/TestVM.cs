﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows;
using CustomControls;

namespace TestApp
{
    class TestVM : INotifyPropertyChanged
    {
        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        
        #endregion

        private ObservableCollection<CCBParentItem> _chadItems = new ObservableCollection<CCBParentItem>
            {
                new CCBParentItem(1, "Assesment", new List<CCBChildItem>
                {
                    new CCBChildItem("10030673", "Assessing During encounter", null),
                    new CCBChildItem("10030618", "Assessing Health & Social care needs", null),
                    new CCBChildItem("10030687", "Assessing on Admission", null)
                }),

                new CCBParentItem(2, "Bladder / Bowel Care", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),
                    new CCBChildItem("10031879", "Managing Urinary Incontinence", null),
                    new CCBChildItem("10032150", "Nephrostomy Care", null),
                    new CCBChildItem("10032788", "Stoma Care", null),
                    new CCBChildItem("10032987", "Teaching about Nephrostomy care", null),
                    new CCBChildItem("10033055", "Teaching about stoma care", null),
                    new CCBChildItem("10033135", "Teaching self catheterisation", null)
                }),

                new CCBParentItem(3, "More Items", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10033135", "Teaching self catheterisation", null)
                }),

                new CCBParentItem(4, "Even More Items", new List<CCBChildItem>
                {
                    
                    new CCBChildItem("10032987", "Teaching about Nephrostomy care", null),
                    new CCBChildItem("10033055", "Teaching about stoma care", null),
                    new CCBChildItem("10033135", "Teaching self catheterisation", null)
                }),

                new CCBParentItem(5, "I'm getting bored", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10032150", "Nephrostomy Care", null),
                    new CCBChildItem("10032788", "Stoma Care", null),
                    new CCBChildItem("10032987", "Teaching about Nephrostomy care", null),
                    new CCBChildItem("10033055", "Teaching about stoma care", null),
                    new CCBChildItem("10033135", "Teaching self catheterisation", null)
                }),

                new CCBParentItem(6, "I'll stop eventually ", new List<CCBChildItem>
                {
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),
                    new CCBChildItem("10031879", "Managing Urinary Incontinence", null),
                    new CCBChildItem("10032150", "Nephrostomy Care", null),
                    new CCBChildItem("10032788", "Stoma Care", null),
                    new CCBChildItem("10032987", "Teaching about Nephrostomy care", null),
                    new CCBChildItem("10033055", "Teaching about stoma care", null),
                    new CCBChildItem("10033135", "Teaching self catheterisation", null)
                }),

                new CCBParentItem(7, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                }),

                new CCBParentItem(8, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                }),

                new CCBParentItem(9, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                }),

                new CCBParentItem(10, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                }),

                new CCBParentItem(11, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                }),

                new CCBParentItem(12, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                }),

                new CCBParentItem(13, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                }),

                new CCBParentItem(14, "Probably about now", new List<CCBChildItem>
                {
                    new CCBChildItem("10030558", "Assessing Bowel continence", null),
                    new CCBChildItem("10030781", "Assessing Urinary Continence", null),
                    new CCBChildItem("10030884", "Catheterising Bladder", null),
                    new CCBChildItem("10041427", "Managing Defaecation", null),
                    new CCBChildItem("10031782", "Managing Encopresis", null),
                    new CCBChildItem("10031805", "Managing Enuresis", null),

                })
            };

        private CCBChildItem _selectedChadItem;

        public CCBChildItem SelectedChadItem
        {
            get { return _selectedChadItem; }
            set
            {
                _selectedChadItem = value;
                OnPropertyChanged("SelectedChadItem");
            }
        }

        public ObservableCollection<CCBParentItem> ChadItems
        {
            get { return _chadItems; }
            set
            {
                _chadItems = value;
                OnPropertyChanged("ChadItems");
            }
        }

        /*
        private ObservableCollection<string> _selectedAnimals;
        public ObservableCollection<string> SelectedAnimals
        {
            get
            {
                if (_selectedAnimals == null)
                {
                    _selectedAnimals = new ObservableCollection<string> { "Dog", "Lion", "Lizard" };
                    SelectedAnimalsText = WriteSelectedAnimalsString(_selectedAnimals);
                    _selectedAnimals.CollectionChanged +=
                        (s, e) =>
                        {
                            SelectedAnimalsText = WriteSelectedAnimalsString(_selectedAnimals);
                            OnPropertyChanged("SelectedAnimals");
                        };
                }
                return _selectedAnimals;
            }
            set
            {
                _selectedAnimals = value;
            }
        }

        public string SelectedAnimalsText
        {
            get { return _selectedAnimalsText; }
            set 
            { 
                _selectedAnimalsText = value;
                OnPropertyChanged("SelectedAnimalsText");
            }
        } string _selectedAnimalsText;


        private static string WriteSelectedAnimalsString(IList<string> list)
        {
            if (list.Count == 0)
                return String.Empty;

            StringBuilder builder = new StringBuilder(list[0]);

            for (int i = 1; i < list.Count; i++)
            {
                builder.Append(", ");
                builder.Append(list[i]);
            }

            return builder.ToString();
        }

    */
    }
}
