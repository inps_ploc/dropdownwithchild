﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomControls
{
    public class CCBParentItem
    {
        private int _id;

        /// <summary>
        /// Initializes a new instance of the <see cref="CCBParentItem"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="children">The children.</param>
        public CCBParentItem(int id, string displayName, IEnumerable<CCBChildItem> children)
        {
            _id = id;
            OptionName = displayName;
            Children = children;
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        public IEnumerable<CCBChildItem> Children { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string OptionName { get; set; }

        public string Id { get; set; }

        public override string ToString()
        {
            // Temporary Measure, make psuedo - DisplayMemberPath
            return OptionName;
        }
    }
}
