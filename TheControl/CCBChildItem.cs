﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomControls
{
    public class CCBChildItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CCBChildItem"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="childData">The child data.</param>
        public CCBChildItem(string id, string name, object childData)
        {
            Id = id;
            OptionName = name;
            ObjectData = childData;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Gets the name of the option.
        /// </summary>
        public string OptionName { get; }

        /// <summary>
        /// Gets or sets the object data.
        /// </summary>
        public object ObjectData { get; set; }

        public override string ToString()
        {
            return OptionName;
        }
    }
}
