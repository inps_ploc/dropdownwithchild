﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Controls.Primitives;
using System.Reflection;
using System.Runtime.InteropServices;

namespace CustomControls
{  
    /// <summary>
    /// A combo box that supports selecting multiple items and two way binding on the selected items collection.    
    /// </summary>
    [TemplatePart(Name = "PART_labelContentPanel", Type = typeof(Panel))]
    [TemplatePart(Name = "PART_popup", Type = typeof(Popup))]
    [TemplatePart(Name = "PART_childPopup", Type = typeof(Popup))]
    [TemplatePart(Name = "PART_childListBox", Type = typeof(ListBox))]

    public class MultiComboBox : ListBox
    {
        #region Fields

        private Panel _labelPanel;
        private Popup _popup;
        private Popup _childPopup;
        private ListBox _childListBox;

        private string _parentLabelHeader;

        #endregion

        #region Dependency Properties

        /// <summary>
        /// Dependency backing for the MaxDropDownHeight property.
        /// </summary>
        public static readonly DependencyProperty MaxDropDownHeightProperty =
            ComboBox.MaxDropDownHeightProperty.AddOwner(typeof(MultiComboBox));

        /// <summary>
        /// Dependency property backing for IsDropDownOpen.
        /// </summary>
        public static readonly DependencyProperty IsDropDownOpenProperty =
            DependencyProperty.Register("IsDropDownOpen", typeof(bool), typeof(MultiComboBox));

        /// <summary>
        /// The is child drop down open property
        /// </summary>
        public static readonly DependencyProperty IsChildDropDownOpenProperty =
            DependencyProperty.Register("IsChildDropDownOpen", typeof(bool), typeof(MultiComboBox));

        /// <summary>
        /// The selected child item property
        /// </summary>
        public static readonly DependencyProperty SelectedChildItemProperty =
            DependencyProperty.Register("SelectedChildItem", typeof(CCBChildItem), typeof(MultiComboBox));

        #endregion


        static MultiComboBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MultiComboBox), new FrameworkPropertyMetadata(typeof(MultiComboBox)));
        }

        /// <summary>
        /// Create bindings and event handlers on named items.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _labelPanel = Template.FindName("PART_labelContentPanel", this) as Panel;
            _popup = Template.FindName("PART_popup", this) as Popup;
            _childPopup = Template.FindName("PART_childPopup", this) as Popup;
            _childListBox = Template.FindName("PART_childListBox", this) as ListBox;

            if (_childListBox != null)
            {
                _childListBox.SelectionChanged += OnChildSelectionChanged;
            }

            _parentLabelHeader = string.Empty;

            LoadLabelContents();
        }


        #region Properties

        /// <summary>
        /// Gets or sets the selected child item.
        /// </summary>
        public CCBChildItem SelectedChildItem
        {
            get { return (CCBChildItem) GetValue(SelectedChildItemProperty); }
            set { SetValue(SelectedChildItemProperty, value);}
        }

        /// <summary>
        /// Gets or sets the value indicating whether the drop down is open.
        /// </summary>
        public bool IsDropDownOpen
        {
            get { return (bool)GetValue(IsDropDownOpenProperty); }
            set { SetValue(IsDropDownOpenProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance's child dropdown is open
        /// </summary>
        public bool IsChildDropDownOpen
        {
            get { return (bool)GetValue(IsChildDropDownOpenProperty); }
            set { SetValue(IsChildDropDownOpenProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating the maximium height of the drop down.
        /// </summary>
        public double MaxDropDownHeight
        {
            get { return (double)GetValue(MaxDropDownHeightProperty); }
            set { SetValue(MaxDropDownHeightProperty, value); }
        }

        public IEnumerable<CCBChildItem> AvailableChildren { get; set; }

        #endregion

        #region Overridden events (from ListBox)

        /// <summary>
        /// Responds to a list box selection change by raising a <see cref="E:System.Windows.Controls.Primitives.Selector.SelectionChanged" /> event.
        /// </summary>
        /// <param name="e">Provides data for <see cref="T:System.Windows.Controls.SelectionChangedEventArgs" />.</param>
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            // Update label contents when selection changes.
            LoadLabelContents();
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Mouse.PreviewMouseDown" /> attached routed event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.MouseButtonEventArgs" /> that contains the event data. The event data reports that one or more mouse buttons were pressed.</param>
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            if (!IsDropDownOpen)
            {
                return;
            }

            // Set Handled here, popup doesn't recieve the click event so it stays open.
            e.Handled = true;

            var toClose = HandleParentPreviewMouseDown();

            // If execution reaches here, the mouse was not over any ListBoxItems, so it is either over the toggle button or the child popup. If the latter, we want the 
            // mouse click to go through so the child listbox can respond. (It in turn handles the event so the popup stays open.)
            toClose = toClose && _popup.Child.IsMouseOver;
            toClose = toClose && _childPopup.Child.IsMouseOver;

            if (toClose)
            {
                e.Handled = false;
            }

        }

        /// <summary>
        /// Open the drop down with the enter key.
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            // ToDo : handle this completely.
            if (e.Key == Key.Enter)
            {
                IsDropDownOpen = !IsDropDownOpen;
                e.Handled = true;
            }
            else
            {
                base.OnKeyDown(e);
            }
        }

        #endregion

        #region Custom Events

        /// <summary>
        /// Called when the child's contents have been changed, this is raised when the ItemsSource is changed or when the user selects an individual element
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnChildSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var addedItems = e.AddedItems;
            // If an item has been added, this means that the user has selected a listItem
            if (addedItems.Count > 0)
            {
                var selectedItem = addedItems[0];

                System.Diagnostics.Debug.WriteLine($"Selected item is {selectedItem}, type is {selectedItem.GetType()}");
                SelectedChildItem = selectedItem as CCBChildItem;

                LoadLabelContents();

                IsDropDownOpen = false;
                IsChildDropDownOpen = false;
            }
            // Conversely, if an item has been removed, this means that the user has selected a new parent, which changes the collection, thus the child drop down should close as it no longer contains appropriate data
            else if (e.RemovedItems.Count > 1)
            {
                IsChildDropDownOpen = false;
            }
        }

        /// <summary>
        /// Returns true if a parent match is found, popup should stay open
        /// </summary>
        /// <returns>true if a parent match is found, popup should stay open</returns>
        private bool HandleParentPreviewMouseDown()
        {
            // Click event will not reach the list box, so need to manually select or 
            // unselect the item which is under the current mouse position.
            foreach (object item in Items)
            {
                ListBoxItem listBoxItem = (ListBoxItem)ItemContainerGenerator.ContainerFromItem(item);
                if (listBoxItem != null && listBoxItem.IsMouseOver)
                {
                    // Find child item, 
                    IsChildDropDownOpen = true;

                    var parentItem = listBoxItem.Content as CCBParentItem;
                    if (parentItem != null)
                    {
                        AvailableChildren = parentItem.Children;

                        _childListBox.ItemsSource = AvailableChildren;
                        _parentLabelHeader = parentItem.OptionName;
                    }

                    return false; // exit, no more to do.
                }
            }

            return true;
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Sets the display contents for the label. Inserts the DisplaySeparator object beteen items.
        /// </summary>
        private void LoadLabelContents()
        {
            // Ignore if the panel is not present.
            if (_labelPanel == null)
            {
                return;
            }

            // Clear current contents.
            _labelPanel.Children.Clear();

            if (SelectedChildItem != null)
            {
                ContentControl labelContentFromChild = new ContentControl
                {
                    IsTabStop = false,
                    Content = new Label
                    {
                        Content = $"{_parentLabelHeader} - {SelectedChildItem.OptionName}"
                    }
                };

                _labelPanel.Children.Add(labelContentFromChild);
            }
        }

        #endregion // Private
    }
}
